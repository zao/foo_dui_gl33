#!/usr/bin/perl

use warnings;
use strict;
use v5.20;

use FindBin;
use File::Copy;
use File::Path qw( make_path );

my $root = $FindBin::Bin;
my $fb2k = "$root/portable";
die unless (-e $fb2k && -d $fb2k);
die unless (@ARGV == 2);
my $component = $ARGV[0];
my $srcbase = $ARGV[1];
my $dstdir = "$fb2k/user-components/$component";

make_path($dstdir);

sub deploy {
	my $ext = shift;
	my $fn = "$srcbase$ext";
	if (-e $fn) {
		copy($fn, $dstdir) or die;
	}
}

deploy ".dll";
deploy ".exe";
deploy ".pdb";