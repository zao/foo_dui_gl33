#include <ATLHelpers.h>
#include <atlframe.h>

#pragma comment(lib, "winmm.lib")

#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#include <algorithm>
#include <atomic>
#include <iterator>
#include <mutex>
#include <random>
#include <memory>
#include <thread>
#include <vector>

#include "Vis.h"
#include <MathGeoLib.h>

// {DD2F328B-6000-47F6-9EC3-CBE89154BCD6}
static const GUID g_visGUID = 
{ 0xdd2f328b, 0x6000, 0x47f6, { 0x9e, 0xc3, 0xcb, 0xe8, 0x91, 0x54, 0xbc, 0xd6 } };

static const GUID g_visSubclassGUID = ui_element_subclass_playback_visualisation;

class WindowInstance : public ui_element_instance, public CWindowImpl<WindowInstance> {
	virtual void set_configuration(ui_element_config::ptr data) override {
	}

	virtual ui_element_config::ptr get_configuration() override {
		ui_element_config::ptr ret = ui_element_config::g_create_empty(g_visGUID);
		return ret;
	}

	virtual bool edit_mode_context_menu_test(const POINT & point, bool isFromKeyboard) override { return true; }

public:
	void initialize_window(HWND parent) {
		this->Create(parent, 0, 0, WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN | WS_CLIPSIBLINGS);
		auto wnd = (uintptr_t)(HWND)*this;
		vis = MakeVis(wnd);
		ColorBundle colors = {
			FromRGBColorRefToFloat4(cb->query_std_color(ui_color_text)),
			FromRGBColorRefToFloat4(cb->query_std_color(ui_color_background)),
			FromRGBColorRefToFloat4(cb->query_std_color(ui_color_selection)),
			FromRGBColorRefToFloat4(cb->query_std_color(ui_color_text))
		};
		vis->UpdateColors(colors);
	}

	static GUID g_get_guid() {
		return g_visGUID;
	}

	static GUID g_get_subclass() {
		return g_visSubclassGUID;
	}

	static void g_get_name(pfc::string_base& out) {
		out.set_string("GL33");
	}

	static ui_element_config::ptr g_get_default_configuration() {
		ui_element_config::ptr ret = ui_element_config::g_create_empty(g_get_guid());
		return ret;
	}

	static char const* g_get_description() {
		return "";
	}

	virtual void notify(GUID const& what, t_size param1, void const* param2, t_size param2Size) override {
		if (what == ui_element_notify_colors_changed) {
			if (vis) {
				ColorBundle colors = {
					FromRGBColorRefToFloat4(cb->query_std_color(ui_color_text)),
					FromRGBColorRefToFloat4(cb->query_std_color(ui_color_background))
				};
				vis->UpdateColors(colors);
			}
			Invalidate();
		}
	}

	ui_element_config::ptr conf;
	ui_element_instance_callback_ptr cb;

public:
	WindowInstance(ui_element_config::ptr conf, ui_element_instance_callback_ptr cb)
		: conf(conf)
		, cb(cb)
	{}

public:
	DECLARE_WND_CLASS_EX(L"foo_dui_gl33", CS_VREDRAW | CS_HREDRAW | CS_OWNDC, 0)

	BEGIN_MSG_MAP_EX(WindowInstance)
	MSG_WM_ERASEBKGND(OnWM_ERASEBKGND);
	MSG_WM_RBUTTONUP(OnWM_RBUTTONUP);
	END_MSG_MAP()

	auto OnWM_ERASEBKGND(CDCHandle dc) -> LRESULT {
		Invalidate(FALSE);
		return 1;
	}

	service_ptr_t<visualisation_stream_v2> visStream;

	auto OnWM_RBUTTONUP(UINT flags, CPoint point) -> void {
		if (cb->is_edit_mode_enabled()) {
			SetMsgHandled(FALSE);
		}
	}

	std::shared_ptr<VisHandle> vis;
};

static service_factory_t<ui_element_impl<WindowInstance>> g_windowElementFactory;

DECLARE_COMPONENT_VERSION("GL33", "1.0", "zao")
VALIDATE_COMPONENT_FILENAME("foo_dui_gl33.dll")