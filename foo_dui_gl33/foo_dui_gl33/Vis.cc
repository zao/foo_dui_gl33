#include "Vis.h"
#include "../SDK/foobar2000.h"

#include <Windows.h>

#define GL3W_CONTEXT_METHOD activeGL3WContext
#include "GL/gl3w.h"
#include <gl/GL.h>

#include <algorithm>
#include <atomic>
#include <mutex>
#include <random>
#include <thread>

static bool operator != (RECT a, RECT b) {
	return a.left != b.left || a.top != b.top || a.right != b.right || a.bottom != b.bottom;
}

thread_local GL3WContext* activeGL3WContext{};

struct Vis : VisHandle {
	HWND wnd;
	HDC dc;
	HGLRC rc;

	ColorBundle colors;
	service_ptr_t<visualisation_stream_v3> visStream;

	Vis(uintptr_t wnd)
		: wnd((HWND)wnd)
	{
		Start();
	}

	~Vis() {
		Stop();
	}

	void ReleaseResolutionIndependentResources() {}

	void GetWholeFile(char const* path, std::vector<char>& sink) {
		auto h = CreateFileA(path, GENERIC_READ, 0, nullptr, OPEN_EXISTING, 0, nullptr);
		if (!h) {
			sink.clear();
			return;
		}
		LARGE_INTEGER cb;
		GetFileSizeEx(h, &cb);
		if (cb.QuadPart == 0) {
			sink.clear();
			CloseHandle(h);
			return;
		}
		sink.resize(cb.LowPart);
		DWORD nRead = 0;
		ReadFile(h, sink.data(), cb.LowPart, &nRead, nullptr);
		CloseHandle(h);
	}

	void CreateResolutionIndependentResources() {}

	GL3WContext extensionContext{};

	std::atomic<bool> shutdownRenderFlag{ false };
	std::unique_ptr<std::thread> renderThread;

	void InitGL(size_t windowWidth, size_t windowHeight) {
		PIXELFORMATDESCRIPTOR pfd = {
			sizeof(PIXELFORMATDESCRIPTOR),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
			PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0, 0, 0,
			0,
			0, 0, 0, 0,
			24,
			8,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
		};
		dc = GetDC(wnd);
		auto pf = ChoosePixelFormat(dc, &pfd);
		SetPixelFormat(dc, pf, &pfd);
		HGLRC tempContext = wglCreateContext(dc);
		wglMakeCurrent(dc, tempContext);

		using _wglCreateContextAttribsARB = HGLRC (WINAPI *)(HDC, HGLRC, int const*);
		_wglCreateContextAttribsARB const wglCreateContextAttribsARB = reinterpret_cast<_wglCreateContextAttribsARB>(wglGetProcAddress("wglCreateContextAttribsARB"));

		int const WGL_CONTEXT_MAJOR_VERSION_ARB = 0x2091;
		int const WGL_CONTEXT_MINOR_VERSION_ARB = 0x2092;
		int const WGL_CONTEXT_LAYER_PLANE_ARB = 0x2093;
		int const WGL_CONTEXT_FLAGS_ARB = 0x2094;
		int const WGL_CONTEXT_PROFILE_MASK_ARB = 0x9126;

		int const WGL_CONTEXT_DEBUG_BIT_ARB = 0x0001;
		int const WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB = 0x0002;

		int const WGL_CONTEXT_CORE_PROFILE_BIT_ARB = 0x00000001;
		int const WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB = 0x00000002;

		int attribs[] = {
			WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
			WGL_CONTEXT_MINOR_VERSION_ARB, 3,
			WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
			0
		};
		rc = wglCreateContextAttribsARB(dc, nullptr, attribs);
		wglMakeCurrent(dc, rc);
		gl3wInit(&extensionContext);
		activeGL3WContext = &extensionContext;
		wglDeleteContext(tempContext);
	}

	void RenderMain() {
		{
			static_api_ptr_t<visualisation_manager> vm;
			vm->create_stream(visStream, visualisation_manager::KStreamFlagNewFFT);
		}

		RECT clientRect;
		GetClientRect(wnd, &clientRect);
		size_t const WindowWidth = clientRect.right ? clientRect.right : 8;
		size_t const WindowHeight = clientRect.bottom ? clientRect.bottom : 8;
		InitGL(WindowWidth, WindowHeight);

		CreateResolutionIndependentResources();
		CreateResolutionDependentResources();

		DWORD const MDT = 1000/120;
		DWORD tic = timeGetTime();
		DWORD timePassed = 0;
		timeBeginPeriod(1);

		while (!shutdownRenderFlag) {
			DWORD toc = timeGetTime();
			int framesDone = 0;
			while (toc - tic >= MDT) {
				float dt = MDT / 1000.0f;
				tic += MDT;
				timePassed += MDT;
				if (++framesDone == 10) {
					tic += (toc - tic) % MDT;
					break;
				}
			}
			RECT currentClientRect;
			GetClientRect(wnd, &currentClientRect);
			if (currentClientRect != clientRect) {
				ReleaseResolutionDependentResources();
				glViewport(0, 0, currentClientRect.right, currentClientRect.bottom);
				CreateResolutionDependentResources();
				clientRect = currentClientRect;
			}
			float4 clearColor = colors.bg;
			{
				double t = 0.0f;
				if (visStream->get_absolute_time(t)) {
					audio_chunk_fast_impl chunk;
					visStream->get_chunk_absolute(chunk, t, MDT / 1000.0f);
					float avg = 0.0f;
					float const* frame = chunk.get_data();
					auto nch = chunk.get_channel_count();
					auto const n = chunk.get_sample_count();
					for (size_t i = 0; i < n; ++i) {
						avg += std::fabs(*frame);
						frame += nch;
					}
					clearColor = Lerp(colors.bg, colors.fg, sqrt(avg / n));
				}
			}
			glClearColor(clearColor.x, clearColor.y, clearColor.z, clearColor.w);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			SwapBuffers(dc);
		}
		timeEndPeriod(1);
		ReleaseResolutionIndependentResources();
		ReleaseResolutionDependentResources();
		wglMakeCurrent(dc, nullptr);
		wglDeleteContext(rc);
	}

	void ReleaseResolutionDependentResources() {}

	void CreateResolutionDependentResources() {
		HRESULT hr = S_OK;
		RECT clientRect;
		GetClientRect(wnd, &clientRect);
		size_t const WindowWidth = clientRect.right;
		size_t const WindowHeight = clientRect.bottom;

		Frustum frustum;
		frustum.SetKind(FrustumSpaceGL, FrustumLeftHanded);
		float const AspectRatio = (float)WindowWidth / (float)WindowHeight;
		frustum.SetVerticalFovAndAspectRatio(1.0f, AspectRatio);
		frustum.SetViewPlaneDistances(1.0f, 250.0f);
		{
			float3 eye{ 0.0f, 0.0f, 25.0f };
			float3 at{ 0.0f, 0.0f, 0.0f };
			float3x4 Camera = float3x4::LookAt(eye, at, -float3::unitZ, float3::unitY, float3::unitY);
			frustum.SetWorldMatrix(Camera);
		}
	}

	void Start() {
		renderThread = std::make_unique<std::thread>([this] { RenderMain(); });
	}

	void Stop() {
		shutdownRenderFlag = true;
		renderThread->join();
	}
	
	virtual void UpdateColors(ColorBundle colors) override {
		this->colors = colors;
	}
};

std::shared_ptr<VisHandle> MakeVis(uintptr_t wnd) {
	auto ret = std::make_shared<Vis>(wnd);
	return ret;
}