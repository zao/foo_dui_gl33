#pragma once

#include <stdint.h>
#include <MathGeoLib.h>

static inline float4 FromRGBColorRefToFloat4(uint32_t c) {
	float4 ret = {
		((c>>0) & 0xFF) / 255.0f,
		((c>>8) & 0xFF) / 255.0f,
		((c>>16) & 0xFF) / 255.0f,
		1.0f
	};
	return ret;
}

static inline float4 FromRGBAColorRefToFloat4(uint32_t c) {
	float4 ret = {
		((c>>0) & 0xFF) / 255.0f,
		((c>>8) & 0xFF) / 255.0f,
		((c>>16) & 0xFF) / 255.0f,
		((c>>24) & 0xFF) / 255.0f
	};
	return ret;
}

struct ColorBundle {
	float4 fg, bg, selection, text;
};

struct VisHandle {
	virtual void UpdateColors(ColorBundle colors) = 0;
};

std::shared_ptr<VisHandle> MakeVis(uintptr_t wnd);